    LIBS/Estrutura do projeto:
      - Stripe: API de pagamentos, controle de fluxo de pagamentos;
      - Github (OAuth): O usuario podera fazer autenticação;
        - OAuth: é uma metodologia de autenticação; Consiste em fazer o login/autenticação no site requisitado( Github, Google, Instagram, Facebook, etc...), e apartir do sucesso da autenticação, permitira que a Aplicação tenha acesso aos dados do usuario, mantendo-os em um estado;
      - FaunaDB: Banco de dados ( REST ), é um banco de dados para apicações SERVERLES; 
        - ** SERVERLES **: Cada rota da nossa aplicação sera execudada em um ambiente isolado;
      - Prismic CMS: Content Management System, Painel de adminidtrsação para poder cadastrar informações e poder servir esses dados atravez de uma API;

    FLUXO DO SITE:  
        - Usuario -> Homepage -> usuario pode fazer Autenticação -> Acesso ao Github -> O usuario pode fazer a inscrição no Stripe -> usuario redirecionado de volta para a aplicação e a Aplicação salvara as informações da inscrição (Salvar no FaunaDB) -> O Usuario podera consumir os dados dos posts -> Prismic devera retornar todos os dados do post, conforme a autenticação do usuario e as regras de negocio caso o usuario não esteja autenticado;

    Oque é Next? "Next é um framework criado em cima do React" - Diego Rocketseat
        - Exemplo: React(SPA: Single Page Aplication/CSR: Client Side Rendering):
          - quando o usuario acessa o browser, a aplicação é contruida em tempo de execução, ou seja é contruida durante o acesso do usuario;
        - Fluxo: Usuario -> Homepage -> requisição no BD -> BD retorna os dados em JSON para a Homepage -> Homepage constroi em tempo de execução as informações para o usuario; 

        - Next(SSR: Server Side Rendering): 
          - quando o usuario acessar a pagina, o serviço do next fará todas as requisições previamente estabelecidas, e trará de prontidão as estruturas/infromações vindas do back-end
          - Fluxo: Usuario -> Homepage ainda em Loading -> Serviço next faz requisições no Back-end -> recebe as respostas do Back-end e monta as informações -> Homepage sai de estado de Loading com os dados ja montados em tela;
          - O Next usa um conceito de rotas chamado File System Rooting, usando do nome das paginas na pasta pages como rotas 

      - getServerSideProps: 
        - é uma função que faz a busca dos dados atravez de requisições em apis, e assim fazendo o papel do serviço next, renderizando as informações dinamicas com funcionamento SSR;
        - pronto ruim: se por algum acaso, 1000000 (1 Milhão) de usuarios acessarem a aplicação dentro de 1 segundo, essa função pro fazer uma requisição a cada acesso, ira matar o BD, por isso é apenas recomendado o uso no casoo de informações dinamicas importantes, para que não haja prejuizo no desenpenho da aplicação;
        - Usado pricipalmente quando preciso da indexaçao, porem usado fortemente para informaçoes dinamicas, que constantemente atualizao; ex: sessao de usuario, informaçoes em tempo real do usuario que esta acessando, contexto da requisiçao...;

      - getStaticProps: 
        - é uma função que faz busca uma vez dos dados atravez de requisições em apis, armazenando essas informações por um determinado tempo e ao chegar ao fim, o next chamará essa função novamente e revalidará essas mesmas informações;
        - Usado em casos é possivel gerar HTLM afim de compartilhar com todos os usuarios a mesma informção (informações que ficarão estaticas por um periodo de tempo, raramente são alteradas) ex: Home de um blog, pagina de um produto, pagina de uma categoria de um produto;

      - Client-side Generating: 
        - utilizado quando nao e preciso de indexaçao, carregado atravez de alguma açao do usuario, informaçao que nao tem necessidade de estar previamente presente quando a pagina e carregada;]

      - Exemplo de uso SSG, SSR, CSG: Posts de um blog
        - Conteudo -> Precisa ser (SSG) -> os diversos prostos, seu titulos, conteudos, imagens raramente serao mudados, por isso e fortemente recomendado o uso de SSG;
        - Comentarios -> Precisa ser (CSG) -> por nao haver uma necessidade de ter os comentarios previamente carregados antes que seja exibido ao cliente, ele pode ser exibido apos o todo o conteudo mais importante ser carregado;
        - 
// Libs a se aprender
  - Next Images